.. testdoc documentation master file, created by
   sphinx-quickstart on Wed Dec 14 11:14:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================
     Bonjour 
=================

Bienvenue dans ma doc
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. image:: logo.gif

MGR-Test
============

.. index:: contenu

Test de documentation Dorian

El Senator

.. glossary::

   On Test

       Ceci est un test

   On test encore

       On est encore en train de tester

   Bijour

       Why don't you back it up with a source senator



MGR
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`